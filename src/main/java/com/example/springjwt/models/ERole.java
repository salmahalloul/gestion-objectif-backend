package com.example.springjwt.models;

public enum ERole {
    ROLE_USER,
    ROLE_MANAGER,
    ROLE_COLLABORATER;
}
